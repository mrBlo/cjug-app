package com.iam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CjugStudyApplication {

    public static void main(String[] args) {
        SpringApplication.run(CjugStudyApplication.class, args);
    }

}
